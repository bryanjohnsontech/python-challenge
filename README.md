# Forestreet Python Coding Challenge #

When completing this coding challenge, please organise your code as if it were going into production, then send us a link to the hosted repository (e.g. Github, Bitbucket...).

### Functional Spec ###
Create a system that will count the number of words in a sentence using a worker and a job queue. There should be two services. One that receives the request and a worker that computes the answer.
The user should be able to:

- Post a sentence to an endpoint
- Get back the number of words

### Hosting ###
We will run your code locally and you can assume docker, docker-compose is installed. Please provide instructions on how to do this. Also think how and where you would deploy your code for discussion during the interview.

### How We Review ###
We value quality over feature-completeness. It is fine to leave things aside provided you explain your reasoning. You should consider this code ready for final review with your colleague, i.e. this would be the last step before deploying to production. 